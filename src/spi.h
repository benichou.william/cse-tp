#include "globalInclude.h"
#include "uart.h"

#ifndef SPI_H_
#define SPI_H_


void initSPI();

byte sendRecieveSPI(byte contents);
void setSPIBaudrate();

#endif