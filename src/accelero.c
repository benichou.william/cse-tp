#include "accelero.h"

//Accelero result registers addresses
const byte ADDR_X_0 = 0x32;
const byte ADDR_X_1 = 0x33;
const byte ADDR_Y_0 = 0x34;
const byte ADDR_Y_1 = 0x35;
const byte ADDR_Z_0 = 0x36;
const byte ADDR_Z_1 = 0x37;


//A litle bit overkill, but at least it is pretty clear
byte createBaseRequest(){
    return 0;
}

//Set request as read or write mode
byte setWriteMode(byte base, bool isWriteMode){
    if(isWriteMode){
        base &= 0x7F;
    }
    else{
        base |= 0x80;
    }
		return base;
}

//Must follow a write request, will write the requested data in the previous assigned register
byte createSetDataRequest(byte dataValue){
    byte transferValue = createBaseRequest();
    transferValue |= dataValue;
	return transferValue;
}

//Specify to the accelerometer that we want to configure a register on next exchange
byte createRegisterConfigRequest(byte registerAddr){
    byte transferValue = createBaseRequest();
    transferValue = setWriteMode(transferValue, true);
    transferValue |= registerAddr;
	return transferValue;
}

//shortcut for sending a read request and recieive the result in one operation
byte readRegister(byte targetRegister){
    byte req;
    byte retValue;

    enableTransfer(true);
    req = createBaseRequest();
    req = setWriteMode(req, false);
    req |= targetRegister;
	sendRecieveSPI(req);
    retValue = sendRecieveSPI(req);
    enableTransfer(false);
    return retValue;
    

}

void setAcceleroConfig(){

    initSPI(); //uses UART, so we need to init UART BEFORE
    setSPIBaudrate();

    enableTransfer(true);

    //0x31 register setup
    sendRecieveSPI(createRegisterConfigRequest(0x31));
    sendRecieveSPI(createSetDataRequest(0x21));

    enableTransfer(false);
    enableTransfer(true);

    //0x2C register setup
    sendRecieveSPI(createRegisterConfigRequest(0x2C));
    sendRecieveSPI(createSetDataRequest(0x0A));

    enableTransfer(false);
    enableTransfer(true);

    //0x2D register setup
    sendRecieveSPI(createRegisterConfigRequest(0x2D));
    sendRecieveSPI(createSetDataRequest(0x2B));

    enableTransfer(false);
}

//return linear interpolation between a and b depending on t. 0 <= t <= 1;
// t = 1 -> return b
// t = 0 -> return a
float lerp(float t, float a, float b){
    return ((b-a) * t) + a;
}

//return a float between 0 and 1 as an inverse interpolation between a and b bounds
float inverseLerp16bit(int val, int a, int b){
    if(val < a) val = a;
    if(val > b) val = b;
    if(a == b) return 0.5F;
    return (float)(val - a) / (float)(b - a);
}


//remap implementation specific to our use case with the accelerometer
float remap16bitToFloat(int startValue, int i_a, int i_b, float o_a, float o_b){
    return lerp(
        inverseLerp16bit(startValue, i_a, i_b), 
        o_a, 
        o_b
        );
}

//combine two bytes to a 16bits value 
float convertToAcceleration(byte a, byte b){
    int bitCode16 = 0;
    bitCode16 |= b;
    bitCode16 = bitCode16 << 8;
    bitCode16 |= a;
    if(bitCode16 <= 0x0FFF)
        return remap16bitToFloat(bitCode16,0, 0x0FFF, 0, 1599);
    else
        return remap16bitToFloat(bitCode16, 0xF000, 0xFFFF, 0, -1600);
}


AcceleroResult getAcceleroValue(){
		byte x_0, x_1, y_0, y_1, z_0, z_1;	
    AcceleroResult result;
	
    x_0 = readRegister(ADDR_X_0);
    x_1 = readRegister(ADDR_X_1);
    y_0 = readRegister(ADDR_Y_0);
    y_1 = readRegister(ADDR_Y_1);
    z_0 = readRegister(ADDR_Z_0);
    z_1 = readRegister(ADDR_Z_1);

    result.x = convertToAcceleration(x_0, x_1);
    result.y = convertToAcceleration(y_0, y_1);
    result.z = convertToAcceleration(z_0, z_1);
   
	return result;
}

byte getAcceleroIdentifier(){
	bool a;
		byte retValue = readRegister(0x00);
	a = 5;
    return retValue;
}

void enableTransfer(bool enabled){
    CS = !enabled;
}
