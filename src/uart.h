#include "globalInclude.h"

#ifndef UART_H_
#define UART_H_
#define STACK_SIZE 2048


void initUART();
void sendNextData();
void sendnUART(char* msg, int size);
void sendUART(char* msg);
void triggerInterruptUART();

#endif