#include "uart.h"
char sendStack[STACK_SIZE];
int stackSize;
int currSendId = 0;
bool sendReady = false;

void initUART(){
    SCON0 &= 0x3F; //UART Mode 1 - clear
	SCON0 |= 0x40; //UART Mode 1 - write
    stackSize = 0;
    currSendId = 0;
	sendReady = true;
}


void triggerInterruptUART()
{
  if(TI0 == 1){
    if(stackSize > 0){
      sendNextData();
    }
		TI0 = 0;		
    
  }
}

//send next element of sendStack
void sendNextData(){
	TI0 = 0;
	if(stackSize > 0){
		sendReady = false;
		stackSize--;
		SBUF0 = sendStack[currSendId];
		currSendId++;
		if(currSendId >= STACK_SIZE) currSendId = 0;
		if(stackSize == 0) sendReady = true;
	}
}

//add message to sendStack, and send first character if not already doing it
void sendnUART(char* msg, int size){
	int i;
	int localId;

	if(stackSize > STACK_SIZE){
		stackSize = STACK_SIZE;
  }
	
	localId = currSendId + stackSize;
  for(i = 0; i < size; i++, localId++){
		if(localId >= STACK_SIZE) 
			localId = 0;
    sendStack[localId] = msg[i];
  }

	stackSize += size;
	
  if(sendReady){
    sendNextData();
  }
	
	

}

//shortcut for sendnUART method
void sendUART(char* msg){
  sendnUART(msg, strlen(msg));
}