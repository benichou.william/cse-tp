#include "spi.h"



void initSPI(){
     //SPI
    SPI0CFG |= 0xC0; //Match accelerometer config
    MSTEN = 1; //Master enable
    SPIEN = 1; //SPI enable
    SPI0CFG |= 0x07; //8 bit shift
}

byte sendRecieveSPI(byte content){

    SPI0DAT = content;
    while(TXBSY){}
    return SPI0DAT;
}

void setSPIBaudrate(){
    SPI0CKR = 255;
}
 