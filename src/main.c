//------------------------------------------------------------------------------------
// Base_8051
//------------------------------------------------------------------------------------
//
// AUTH: DF
// DATE: 18-09-2017
// Target: C8051F020
//
// Tool chain: KEIL UV4
//
// Application de base qui configure seulement l'oscillateur et le watchdog
//-------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------------
#include "globalInclude.h"
#include "uart.h"
#include "accelero.h"

#define NB_ITER 10

sbit LED = P1^6; // Led verte embarqu�e sur la carte
char printBuffer[128];
int fetchTickRemainingTicks = NB_ITER;



void printAccelero();

//------------------------------------------------------------------------------------
// MAIN Routine
//------------------------------------------------------------------------------------
void main (void)
{
	char msgBuffer[32];
	char id;
	
  Init_Device();
  
  //Crossbar config
  P0MDOUT   = 0x3F;
  XBR0      = 0x06;
  XBR1      = 0xA0;
  XBR2      = 0x40;

	initUART();
	
  
  setAcceleroConfig();

	
  EA = 1; //Enable interrupts

	//UART mode 1
  ES0 = 1; //Enable UART interrupt
  

  //Timer enable
  RCLK0 = 1; // Timer 2 baud rate generator mode
  TCLK0 = 1; // Timer 2 baud rate generator mode
  TR2 = 1; // Timer 2 enable

  EIE2 |= 0x01; //interrupt enable
  TMR3CN = 0x04;//Timer 3 enable
	
	RCAP2H = 0xFF; //auto-reload part 2
  RCAP2L = 0xB8; //auto-reload part 1
  
	CKCON |= 0x20;

	sendUART("Hello World!\r\n");

  

  id = getAcceleroIdentifier();
  sprintf(msgBuffer, "Accelero ID:0x%x\r\n", (int)id);
  sendUART(msgBuffer);

  while (1)
  {

  }   

}

//Interrupt called each time a send/receive is performed
// TI0 = 1 when send performed
void onUart0SendReceive() interrupt 4
{
  triggerInterruptUART();
}


//We use this to send accelero values at a convininent speed (prevent buffer overflow when we send faster than UART speed)
void onTimer3Interrupt() interrupt 14
{
	fetchTickRemainingTicks--;
	if(fetchTickRemainingTicks <= 0){
    fetchTickRemainingTicks = NB_ITER;
    printAccelero();

  }
	
	TMR3CN &= 0x7F;
  
  
}

//Use of sprintf to format our values in a human-readable format
void printAccelero()
{
  AcceleroResult acceleroData;
	acceleroData = getAcceleroValue();
  sprintf(printBuffer, "[ACC]{x=%.02f,y=%.02f,z=%.02f}\r\n", acceleroData.x, acceleroData.y, acceleroData.z);
  sendUART(printBuffer);
}

