#ifndef ACCELERO_H_
#define ACCELERO_H_
#include "globalInclude.h"
#include "spi.h"

sbit CS = P1^0;
typedef struct AcceleroResult{
    float x;
    float y;
    float z;
} AcceleroResult;

byte createBaseRequest();

byte setWriteMode(byte base, bool isWriteMode);

byte createSetDataRequest(byte dataValue);

byte createRegisterConfigRequest(byte registerAddr);

byte readRegister(byte targetRegister);

void setAcceleroConfig();

byte getAcceleroIdentifier();

void enableTransfer(bool enabled);

float lerp(float t, float a, float b);

float inverseLerp16bit(int val, int a, int b);

float remap16bitToFloat(int startValue, int i_a, int i_b, float o_a, float o_b);

float convertToAcceleration(byte a, byte b);

AcceleroResult getAcceleroValue();

#endif